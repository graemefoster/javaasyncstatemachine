package com.graemefoster.asyncstatemachine.asyncstatemachine;

public enum ValuationState {

    NEW,
    AT_THIRD_PARTY,
    DONE,
    FAILED

}
