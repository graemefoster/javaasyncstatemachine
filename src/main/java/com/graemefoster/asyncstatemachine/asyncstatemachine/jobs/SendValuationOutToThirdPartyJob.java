package com.graemefoster.asyncstatemachine.asyncstatemachine.jobs;

import com.graemefoster.asyncstatemachine.asyncstatemachine.Valuation;
import com.graemefoster.asyncstatemachine.asyncstatemachine.infrastructure.TransactionQuartzJobBean;
import com.graemefoster.asyncstatemachine.asyncstatemachine.infrastructure.ValuationJobScheduler;
import org.quartz.JobExecutionContext;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class SendValuationOutToThirdPartyJob extends TransactionQuartzJobBean {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ValuationJobScheduler scheduler;

    @Override
    protected void executeJob(JobExecutionContext jobExecutionContext, Valuation valuation) throws SchedulerException {
        logger.info("Sending valuation {} out to third party", valuation.getId());
        valuation.sendToThirdParty(scheduler);
    }
}


