package com.graemefoster.asyncstatemachine.asyncstatemachine.jobs;

import com.graemefoster.asyncstatemachine.asyncstatemachine.Valuation;
import com.graemefoster.asyncstatemachine.asyncstatemachine.infrastructure.TransactionQuartzJobBean;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ValuationInFromThirdParty extends TransactionQuartzJobBean {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    protected void executeJob(JobExecutionContext jobExecutionContext, Valuation valuation) {
        logger.info("Valuation {} came back. Now done!", valuation.getId());
        valuation.backFromThirdParty();
    }
}


