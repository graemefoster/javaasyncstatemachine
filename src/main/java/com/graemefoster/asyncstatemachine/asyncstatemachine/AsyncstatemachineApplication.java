package com.graemefoster.asyncstatemachine.asyncstatemachine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class AsyncstatemachineApplication {

    public static void main(String[] args) {
        SpringApplication.run(AsyncstatemachineApplication.class, args);
    }
}
