package com.graemefoster.asyncstatemachine.asyncstatemachine;

import com.graemefoster.asyncstatemachine.asyncstatemachine.infrastructure.ResourceNotFoundException;
import com.graemefoster.asyncstatemachine.asyncstatemachine.infrastructure.ValuationJobScheduler;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ValuationWebhookController {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ValuationRepository repository;

    @Autowired
    private ValuationJobScheduler scheduler;

    @Transactional
    @GetMapping("/newvaluation")
    public String newvaluation(@RequestParam(name="name") String name) throws SchedulerException {

        Valuation valuation = new Valuation(name);
        repository.add(valuation);

        //and kick off the workflow by sending it out to a 3rd party:
        scheduler.scheduleJob(valuation, "DispatchToThirdParty");

        logger.info("New valuation in for job Id:{}  Name:{}!", valuation.getId(), name);

        if (name.equals("graeme")) throw new RuntimeException("Uh oh!!");

        return "Hello " + name + "!";
    }

    @GetMapping("/valuation/{id}")
    public String get(@PathVariable int id) {

        Valuation v = repository.getById(id);
        if (v != null){
            return "Valuation id " + v.getId() + " Name: " + v.getName() + " status: " + v.getState();
        }
        else {
            throw new ResourceNotFoundException();
        }
    }

    @GetMapping("/valuation")
    public List<Valuation> all() {
        return repository.all();
    }
}

