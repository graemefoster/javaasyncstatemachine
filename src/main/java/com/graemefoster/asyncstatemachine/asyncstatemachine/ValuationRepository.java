package com.graemefoster.asyncstatemachine.asyncstatemachine;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Transactional(rollbackFor = Throwable.class)
@Repository
public class ValuationRepository {

    @Autowired
    private SessionFactory sessionFactory;

    public void add(Valuation v) {
        sessionFactory.getCurrentSession().save(v);
    }

    public void update(Valuation v) {
        sessionFactory.getCurrentSession().update(v);
    }

    public Valuation getById(int id) {
        return sessionFactory.getCurrentSession().get(Valuation.class, id);
    }

    public List<Valuation> all() {
        CriteriaQuery<Valuation> query = sessionFactory.getCurrentSession().getCriteriaBuilder().createQuery(Valuation.class);
        Root<Valuation> variableRoot = query.from(Valuation.class);
        query.select(variableRoot);
        return sessionFactory.getCurrentSession().createQuery(query).list();
    }
}
