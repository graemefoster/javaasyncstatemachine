package com.graemefoster.asyncstatemachine.asyncstatemachine.infrastructure.config;

import com.graemefoster.asyncstatemachine.asyncstatemachine.jobs.SendValuationOutToThirdPartyJob;
import com.graemefoster.asyncstatemachine.asyncstatemachine.jobs.ValuationInFromThirdParty;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import javax.annotation.PostConstruct;

import static org.quartz.JobBuilder.newJob;

@Configuration
public class QrtzSchedulerConfig {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private SchedulerFactoryBean schedulerBean;

    @PostConstruct
    public void init() throws SchedulerException {

        logger.info("Hello world from Quartz...");

        schedulerBean.getScheduler().clear();

        schedulerBean.getScheduler().addJob(dispatchNextJobToThirdParty(), false);
        schedulerBean.getScheduler().addJob(simulateReturnFromThirdParty(), false);

        schedulerBean.getScheduler().start();
    }

    public JobDetail dispatchNextJobToThirdParty() {

        return newJob()
                .ofType(SendValuationOutToThirdPartyJob.class)
                .storeDurably()
                .withIdentity(JobKey.jobKey("DispatchToThirdParty"))
                .withDescription("Send next to third party...").build();
    }

    public JobDetail simulateReturnFromThirdParty() {
        //and start the job to return it in a bit of time
        return newJob()
                .ofType(ValuationInFromThirdParty.class)
                .storeDurably()
                .withIdentity(JobKey.jobKey("ValuationReturnFromThirdParty"))
                .withDescription("Valuation returning back from 3rd party...").build();

    }

}