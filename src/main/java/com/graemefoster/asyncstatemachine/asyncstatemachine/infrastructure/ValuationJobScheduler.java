package com.graemefoster.asyncstatemachine.asyncstatemachine.infrastructure;

import com.graemefoster.asyncstatemachine.asyncstatemachine.Valuation;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.UUID;

@Service
public class ValuationJobScheduler {

    @Autowired
    private Scheduler scheduler;

    //Schedules a job pretty much immediately
    public void scheduleJob(Valuation valuation, String jobKey) throws SchedulerException {

        JobDetail job = scheduler.getJobDetail(JobKey.jobKey(jobKey));

        scheduler
                .scheduleJob(
                        TriggerBuilder
                                .newTrigger()
                                .forJob(job)
                                .usingJobData("id", valuation.getId())
                                .withIdentity(jobKey + "-" + valuation.getId() + "-" + UUID.randomUUID().toString())
                                .startNow()
                                .build());

    }

    //Schedules a job pretty much immediately
    public void scheduleJob(Valuation valuation, String jobKey, int startInSeconds) throws SchedulerException {

        JobDetail job = scheduler.getJobDetail(JobKey.jobKey(jobKey));
        Calendar startTime = Calendar.getInstance();
        startTime.add(Calendar.SECOND, startInSeconds);

        scheduler
                .scheduleJob(
                        TriggerBuilder
                                .newTrigger()
                                .forJob(job)
                                .usingJobData("id", valuation.getId())
                                .withIdentity(jobKey + "-" + valuation.getId() + "-" + UUID.randomUUID().toString())
                                .startAt(startTime.getTime())
                                .build());

    }

}
