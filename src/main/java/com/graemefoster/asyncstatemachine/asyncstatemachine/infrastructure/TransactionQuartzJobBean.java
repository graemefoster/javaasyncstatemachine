package com.graemefoster.asyncstatemachine.asyncstatemachine.infrastructure;

import com.graemefoster.asyncstatemachine.asyncstatemachine.Valuation;
import com.graemefoster.asyncstatemachine.asyncstatemachine.ValuationRepository;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

//Quartz creates the job class so AOP transactions via @Transactional don't work.
//So instead this uses the lo-fi approach of explicitly getting a new transaction.
public abstract class TransactionQuartzJobBean extends QuartzJobBean {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
    private ValuationRepository repository;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        int id = jobExecutionContext.getMergedJobDataMap().getInt("id");
        String jobName = jobExecutionContext.getJobDetail().getKey().getName();

        logger.info("Executing job {} on valuation {}",
                jobName,
                id);

        TransactionDefinition transactionDefinition = new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(transactionDefinition);
        try {
            Valuation valuation= repository.getById(id);

            if (valuation != null) {
                logger.info("Processing job {} for valuation {}.", jobName, id);
                executeJob(jobExecutionContext, valuation);
                logger.info("Processed job {} for valuation {}.", jobName, id);
            } else {
                logger.warn("Failed to find valuation {} to satisfy job {}", id, jobName);
            }

            transactionManager.commit(status);
        } catch (Throwable t) {

            logger.error("Failed executing job {} on valuation {}.",
                    jobName,
                    id);

            transactionManager.rollback(status);

            //If the job has signified exactly how it wants to do next via a JobExecutionException then rethrow
            //it. Otherwise rethrow but don't ask for immediate execution.
            if (t.getClass() == JobExecutionException.class)
            {
                throw (JobExecutionException)t;
            } else {
                throw new JobExecutionException(t, false);
            }
        }
    }

    protected abstract void executeJob(JobExecutionContext jobExecutionContext, Valuation valuation) throws SchedulerException;
}
