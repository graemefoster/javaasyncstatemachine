package com.graemefoster.asyncstatemachine.asyncstatemachine;

import com.graemefoster.asyncstatemachine.asyncstatemachine.infrastructure.ValuationJobScheduler;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.Random;

@SuppressWarnings("unused")
@Entity
@Table(name = "Valuation")
public class Valuation {

    static Logger logger = LoggerFactory.getLogger(Valuation.class);

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int _id;

    @Column(name = "name")
    private String name;

    @Column(name = "state")
    private ValuationState _state;

    @Column(name = "retry")
    private int _retry;

    static Random random = new Random();

    //For hibernate
    protected Valuation() {}

    public Valuation(String name) {
        this.name = name;
        this._state = ValuationState.NEW;
    }

    private int getRetryWait(int... backOffs) {
        return backOffs[_retry];
    }

    public void sendToThirdParty(ValuationJobScheduler scheduler) throws SchedulerException {

        //what if the call failed?!
        int random = Valuation.random.nextInt(10);
        if (random < 3) {
            scheduler.scheduleJob(this, "ValuationReturnFromThirdParty");
            _state = ValuationState.AT_THIRD_PARTY;
        } else {
            _retry++;
            if (_retry < 3) {
                logger.info("Simulating fail. Will try and send again in {} seconds", random);
                scheduler.scheduleJob(this, "DispatchToThirdParty", getRetryWait(1, 2, 5, 10));
            } else {
                logger.info("Failed max times. Moving to failed state", random);
                _state = ValuationState.FAILED;
            }
        }
    }

    public void backFromThirdParty() {
        _state = ValuationState.DONE;
    }


    public ValuationState getState() {
        return _state;
    }

    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public int getRetry() {
        return _retry;
    }
}
